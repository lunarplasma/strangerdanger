#give resistance on splat
scoreboard players add @s[nbt={OnGround:0b}] wasd.air_time 1
effect give @s[scores={wasd.air_time=9..}] minecraft:jump_boost 1 250 true

effect clear @s[nbt={OnGround:1b},scores={wasd.air_time=9..}] minecraft:jump_boost
scoreboard players set @s[nbt={OnGround:1b}] wasd.air_time 0


execute if score drain ring_settings matches 1 run scoreboard players remove @s wasd.magic_meter 2