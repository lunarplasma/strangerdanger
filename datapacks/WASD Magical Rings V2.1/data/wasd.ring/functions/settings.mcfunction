function wasd.lib:util/clear_text

tellraw @s {"text":"Configure Magic Meter Settings","color":"gray","bold":true}

tellraw @s [""]
tellraw @s {"text":"Rings Drain Magic Slowly?","color":"gray"}
tellraw @s [{"text":"[Yes] ","color":"dark_green","bold":true,"hoverEvent":{"action":"show_text","value":"Must have WASD Magic Meter Data Pack installed to work."},"clickEvent":{"action":"run_command","value":"/scoreboard players set drain ring_settings 1"}},{"text":"[No]","color":"dark_red","bold":true,"clickEvent":{"action":"run_command","value":"/scoreboard players set drain ring_settings 0"}}]
