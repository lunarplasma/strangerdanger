scoreboard objectives add wasd_data_packs dummy
scoreboard players set wasd_magic_rings wasd_data_packs 1

scoreboard objectives add wasd.air_time dummy
scoreboard objectives add wasd.lava_timer dummy
scoreboard objectives add wasd.regin dummy
scoreboard objectives add ring_settings dummy
function wasd.ring:error
execute unless score version wasd.lib_setting matches 21.. run schedule function wasd.ring:outdated_lib 1t
function wasd.lib:on_load/text