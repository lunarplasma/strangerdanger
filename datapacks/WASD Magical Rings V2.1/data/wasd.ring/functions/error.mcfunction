tellraw @a {"text":"If you are reading this, that means that you forgot to install the WASD Data Packs Library.","color":"dark_red"}
tellraw @a [""]
tellraw @a {"text":"Download the Library [Click]","color":"dark_green","clickEvent":{"action":"open_url","value":"http://wasdbuildteam.website/data-packs/wasd-libraries/"}}
tellraw @a [""]
tellraw @a [{"text":"Packs by "},{"text":"The WASD Build Team","color":"dark_purple"}]
tellraw @a {"text":"Other Data Packs","color":"dark_green","clickEvent":{"action":"open_url","value":"http://wasdbuildteam.website/Data-Packs/"}}
tellraw @a {"text":"Support us on Patreon","color":"gold","clickEvent":{"action":"open_url","value":"https://www.patreon.com/WASD_Build_Team"}}
tellraw @a {"text":"Join our Discord","color":"dark_aqua","clickEvent":{"action":"open_url","value":"https://discordapp.com/invite/yqk4VfA"}}