#transfer name to the villager entity
summon minecraft:area_effect_cloud ~ 255 ~ {Tags:["wasd.air_detector"]}
execute as @e[tag=wasd.air_detector] at @s run function wasd.villager:find_air



execute at @e[tag=wasd.air_detector] run setblock ~ ~-1 ~ minecraft:acacia_sign{Text1:'[{"selector":"@e[tag=wasd.villager_name,sort=nearest,limit=1]"},{"text":" "},{"selector":"@e[tag=wasd.villager_title,sort=nearest,limit=1]"}]'} replace
execute at @e[tag=wasd.air_detector] run data modify entity @s CustomName set from block ~ ~-1 ~ Text1
execute at @e[tag=wasd.air_detector] run setblock ~ ~-1 ~ air

kill @e[tag=wasd.air_detector]