# Function runs at the beginning of a load

# Starts timers
function blazeandcave:one_second_timer
function blazeandcave:ten_second_timer

# Creates scoreboard objectives the first time this world is created
scoreboard objectives add created dummy
execute unless score created created matches 1 run function blazeandcave:new_world