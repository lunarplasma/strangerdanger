# Function runs once per tick

# Free Diver and Sleep with the Fishes (stay underwater)
execute as @a at @s if block ~ ~ ~ minecraft:water run scoreboard players add @a time_underwater 1
execute as @a at @s unless block ~ ~ ~ minecraft:water run scoreboard players set @s time_underwater 0
execute as @a at @s if block ~ ~1.7 ~ minecraft:air run scoreboard players set @s time_underwater 0
advancement grant @a[scores={time_underwater=2400..}] only blazeandcave:biomes/free_diver
advancement grant @a[scores={time_underwater=24000..}] only blazeandcave:biomes/sleep_with_the_fishes

# Dragon vs Dragon
execute as @a[scores={dvd=1}] unless entity @s[nbt={Inventory:[{Slot:103b,id:"minecraft:dragon_head"}]}] run scoreboard players set @s dvd 0

# Dragon vs Dragon II: Electric Boogaloo
execute as @a[scores={dvd2eb=1}] unless entity @s[nbt={OnGround:0b,Inventory:[{Slot:103b,id:"minecraft:dragon_head"}]}] run scoreboard players set @s dvd2eb 0