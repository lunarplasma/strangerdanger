# Function runs the first time BlazeandCave's Advancements Pack is loaded

scoreboard objectives add Advancements dummy
scoreboard objectives add Reward dummy

# The following are used for statistical advancements
scoreboard objectives add minecart_1cm minecraft.custom:minecraft.minecart_one_cm
scoreboard objectives add pig_1cm minecraft.custom:minecraft.pig_one_cm
scoreboard objectives add horse_1cm minecraft.custom:minecraft.horse_one_cm
scoreboard objectives add elytra_1cm minecraft.custom:minecraft.aviate_one_cm
scoreboard objectives add sneak_1cm minecraft.custom:minecraft.crouch_one_cm
scoreboard objectives add walk_1cm minecraft.custom:minecraft.walk_one_cm
scoreboard objectives add sprint_1cm minecraft.custom:minecraft.sprint_one_cm
scoreboard objectives add jump minecraft.custom:minecraft.jump
scoreboard objectives add swim_1cm minecraft.custom:minecraft.swim_one_cm
scoreboard objectives add cake_slice minecraft.custom:minecraft.eat_cake_slice
scoreboard objectives add day_count dummy
scoreboard objectives add insomniac minecraft.custom:minecraft.time_since_rest
scoreboard objectives add time_underwater dummy
scoreboard objectives add items_enchanted minecraft.custom:minecraft.enchant_item
scoreboard objectives add animals_bred minecraft.custom:minecraft.animals_bred
scoreboard objectives add fill_cauldron minecraft.custom:minecraft.fill_cauldron
scoreboard objectives add clean_armor minecraft.custom:minecraft.clean_armor
scoreboard objectives add clean_banner minecraft.custom:minecraft.clean_banner
scoreboard objectives add clean_shulker minecraft.custom:minecraft.clean_shulker_box
scoreboard objectives add place_painting minecraft.used:minecraft.painting

# The following are used for other advancements
scoreboard objectives add castaway dummy
scoreboard objectives add ten_withers dummy
scoreboard objectives add dvd dummy
scoreboard objectives add dvd2eb dummy
scoreboard objectives add event_horizon dummy
scoreboard objectives add event_death deathCount