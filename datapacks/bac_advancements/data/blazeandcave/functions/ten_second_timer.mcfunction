# Function runs once every ten seconds

# Checks scores for statistics advancements and grants them
advancement grant @a[scores={minecart_1cm=100000..}] only blazeandcave:statistics/minecart_rider
advancement grant @a[scores={minecart_1cm=1000000..}] only blazeandcave:statistics/i_like_trains
advancement grant @a[scores={minecart_1cm=10000000..}] only blazeandcave:statistics/global_train_network
advancement grant @a[scores={pig_1cm=10000..}] only blazeandcave:statistics/pig_training
advancement grant @a[scores={pig_1cm=100000..}] only blazeandcave:statistics/snout_500
advancement grant @a[scores={pig_1cm=1000000..}] only blazeandcave:statistics/lightning_mcpig
advancement grant @a[scores={horse_1cm=100000..}] only blazeandcave:statistics/horse_training
advancement grant @a[scores={horse_1cm=1000000..}] only blazeandcave:statistics/sheriff_firearm
advancement grant @a[scores={horse_1cm=5000000..}] only blazeandcave:statistics/scourge_of_the_west
advancement grant @a[scores={boat_1cm=100000..}] only blazeandcave:statistics/luxury_cruise
advancement grant @a[scores={boat_1cm=1000000..}] only blazeandcave:statistics/pirate_captain
advancement grant @a[scores={boat_1cm=5000000..}] only blazeandcave:statistics/sailor_of_the_seven_seas
advancement grant @a[scores={elytra_1cm=100000..}] only blazeandcave:statistics/take_to_the_skies
advancement grant @a[scores={elytra_1cm=1000000..}] only blazeandcave:statistics/supersonic
advancement grant @a[scores={elytra_1cm=10000000..}] only blazeandcave:statistics/frequent_flyer
advancement grant @a[scores={sneak_1cm=10000..}] only blazeandcave:statistics/sneaky_snitch
advancement grant @a[scores={sneak_1cm=100000..}] only blazeandcave:statistics/black_belt_ninja
advancement grant @a[scores={sneak_1cm=1000000..}] only blazeandcave:statistics/ancient_kung_fu_master
advancement grant @a[scores={walk_1cm=1000000..}] only blazeandcave:statistics/out_for_a_stroll
advancement grant @a[scores={walk_1cm=10000000..}] only blazeandcave:statistics/who_needs_cars
advancement grant @a[scores={walk_1cm=100000000..}] only blazeandcave:statistics/i_enjoy_long_walks_and_playing_minecraft
advancement grant @a[scores={sprint_1cm=4200000..}] only blazeandcave:statistics/marathon
advancement grant @a[scores={sprint_1cm=25000000..}] only blazeandcave:statistics/your_legs_must_be_tired
advancement grant @a[scores={sprint_1cm=100000000..}] only blazeandcave:statistics/did_you_just_run_across_earth
advancement grant @a[scores={jump=1000..}] only blazeandcave:statistics/spring_in_your_step
advancement grant @a[scores={jump=10000..}] only blazeandcave:statistics/boing_boing
advancement grant @a[scores={jump=100000..}] only blazeandcave:statistics/jumping_jacks
advancement grant @a[scores={swim_1cm=100000..}] only blazeandcave:statistics/laps_in_the_pool
advancement grant @a[scores={swim_1cm=1000000..}] only blazeandcave:statistics/olympic_athlete
advancement grant @a[scores={swim_1cm=10000000..}] only blazeandcave:statistics/olympic_gold_medallist
advancement grant @a[scores={items_enchanted=10..}] only blazeandcave:statistics/novice_enchanter
advancement grant @a[scores={items_enchanted=50..}] only blazeandcave:statistics/apprentice_enchanter
advancement grant @a[scores={items_enchanted=250..}] only blazeandcave:statistics/elderly_enchanter
advancement grant @a[scores={animals_bred=100..}] only blazeandcave:statistics/cupid
advancement grant @a[scores={animals_bred=500..}] only blazeandcave:statistics/love_is_in_the_air
advancement grant @a[scores={animals_bred=2500..}] only blazeandcave:statistics/angel_of_love
advancement grant @a[level=30..] only blazeandcave:statistics/level_up
advancement grant @a[level=100..] only blazeandcave:statistics/overlevelling
advancement grant @a[level=1000..] only blazeandcave:statistics/xp_grinder
execute store result score @a day_count run time query day
advancement grant @a[scores={day_count=1..}] only blazeandcave:statistics/the_first_night
advancement grant @a[scores={day_count=100..}] only blazeandcave:statistics/passing_the_time
advancement grant @a[scores={day_count=365..}] only blazeandcave:statistics/happy_birthday

advancement grant @a[scores={day_count=1..}] only blazeandcave:statistics/root

# Other statistics-based advancements
advancement grant @a[scores={cake_slice=100..}] only blazeandcave:statistics/must_be_your_birthday
advancement grant @a[scores={insomniac=720000..}] only blazeandcave:statistics/insomniac

# Gives spiders with skeletons riding them a unique tag for the "Spider-skeleton" advancement
tag @e[type=spider,nbt={Passengers:[{id:"minecraft:skeleton"}]}] add spider_jockey

# The World Is Ending (counts ten withers)
execute as @a at @s store result score @s ten_withers run execute if entity @e[type=wither,distance=..128]
execute as @a if score @s ten_withers matches 10.. run advancement grant @s only blazeandcave:challenges/the_world_is_ending

schedule function blazeandcave:ten_second_timer 10s